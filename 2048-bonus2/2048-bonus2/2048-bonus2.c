#include <stdio.h>
#include <stdlib.h>

void randomNum(int A[4][4]){
	int i, j, row, column, num;
	do {
		row = (rand() % 4);
		column = (rand() % 4);
	} while (A[row][column] != 0);

	num = (rand() % 2 + 1) * 2;
	A[row][column] = num;
	return;
}

void print(int A[4][4]) {
	int i, j;
	for (i = 0; i<4; i++){
		printf("+----+----+----+----+\n");
		for (j = 0; j<4; j++){
			if (A[i][j] == 0){
				printf("|    ");
			}
			else
				printf("|%4d", A[i][j]);
		}
		printf("|\n");
	}
	printf("+----+----+----+----+\n");
	return;
}

int read_command(int mode) {
	int x;
	if (mode == 0){
		do {
			printf("Command: [ LEFT (1) | RIGHT (2) | UP (3) | DOWN (4) | QUIT (5) | UNDO (6) ]?\n");
			scanf("%d", &x);
			if (x <= 0 || x >= 7){
				printf("[Error] The input should between 1 to 6\n");
			}
		} while (x <= 0 || x >= 7);
	}
	if (mode == 1){
		do {
			printf("Command: [ LEFT (1) | RIGHT (2) | UP (3) | DOWN (4) | QUIT (5) ]?\n");
			scanf("%d", &x);
			if (x <= 0 || x >= 6){
				printf("[Error] The input should between 1 to 5\n");
			}
		} while (x <= 0 || x >= 6);
	}
	return x;
}

int change_position(int mode, int x, int A[4][4]){
	int i, j, k, num, row, column, change = 0;
	int B[4][4] = { 0 }, C[4][4] = { 0 }, repeat = 1, stop;
	/*pull left*/
	if (x == 1){
		for (i = 0; i < 4; i++){
			k = 0;
			for (j = 0; j < 4; j++){
				if (A[i][j] != 0){
					B[i][k] = A[i][j];
					k++;
				}
			}
		}
		for (i = 0; i < 4; i++){
			for (j = 1; j < 4; j++){
				if (B[i][j] == B[i][j - 1]){
					B[i][j - 1] = 2 * B[i][j - 1];
					B[i][j] = 0;
				}
			}
		}
		for (i = 0; i < 4; i++){
			k = 0;
			for (j = 0; j < 4; j++){
				if (B[i][j] != 0){
					C[i][k] = B[i][j];
					k++;
				}
			}
		}
	}
	/*pull right*/
	if (x == 2){
		for (i = 0; i < 4; i++){
			k = 3;
			for (j = 3; j >= 0; j--){
				if (A[i][j] != 0){
					B[i][k] = A[i][j];
					k--;
				}
			}
		}
		for (i = 0; i <4; i++){
			for (j = 3; j >= 1; j--){
				if (B[i][j] == B[i][j - 1]){
					B[i][j] = 2 * B[i][j];
					B[i][j - 1] = 0;
				}
			}
		}
		for (i = 0; i < 4; i++){
			k = 3;
			for (j = 3; j >= 0; j--){
				if (B[i][j] != 0){
					C[i][k] = B[i][j];
					k--;
				}
			}
		}
	}
	/*pull up*/
	if (x == 3){
		for (j = 0; j < 4; j++){
			k = 0;
			for (i = 0; i < 4; i++){
				if (A[i][j] != 0){
					B[k][j] = A[i][j];
					k++;
				}
			}
		}
		for (j = 0; j < 4; j++){
			for (i = 0; i < 3; i++){
				if (B[i][j] == B[i + 1][j]){
					B[i][j] = 2 * B[i][j];
					B[i + 1][j] = 0;
				}
			}
		}
		for (j = 0; j < 4; j++){
			k = 0;
			for (i = 0; i < 4; i++){
				if (B[i][j] != 0){
					C[k][j] = B[i][j];
					k++;
				}
			}
		}
	}
	/*pull down*/
	if (x == 4){
		for (j = 0; j < 4; j++){
			k = 3;
			for (i = 3; i >= 0; i--){
				if (A[i][j] != 0){
					B[k][j] = A[i][j];
					k--;
				}
			}
		}
		for (j = 0; j < 4; j++){
			for (i = 3; i >= 1; i--){
				if (B[i][j] == B[i - 1][j]){
					B[i][j] = 2 * B[i][j];
					B[i - 1][j] = 0;
				}
			}
		}
		for (j = 0; j < 4; j++){
			k = 3;
			for (i = 3; i >= 0; i--){
				if (B[i][j] != 0){
					C[k][j] = B[i][j];
					k--;
				}
			}
		}
	}
	for (i = 0; i<4; i++){
		for (j = 0; j<4; j++){
			if (C[i][j] != A[i][j]) change = 1;
		}
	}
	if (change){
		for (i = 0; i<4; i++){
			for (j = 0; j<4; j++){
				A[i][j] = C[i][j];
			}
		}
		if (mode == 0) { randomNum(A); print(A); }
		if (mode == 1) { print(A); }
	}

	if (change == 0){
		printf("[Error] invalid direction\n");
	}
	return 0;
}

int checkb(int A[4][4]){
	int repeat = 0;
	int i, j;
	for (i = 0; i<4; i++){
		for (j = 0; j<4; j++){
			if (A[i][j] == 2048) { return 0; }
		}
	}
	for (i = 0; i<4; i++){
		for (j = 0; j<4; j++){
			if (A[i][j] == 0) { return 1; }
		}
	}
	for (i = 0; i<4; i++){
		for (j = 0; j<3; j++){
			if (A[i][j] == A[i][j + 1]) { return 1; }
		}
	}
	for (i = 0; i<3; i++){
		for (j = 0; j<4; j++){
			if (A[i][j] == A[i + 1][j]) { return 1; }
		}
	}
	return 0;
}

void Gen_Num(int A[4][4]){
	int repeat = 0, row, column, value;
	int again_cell = 0;

	do{
		int again_row = 1;
		while (again_row){
			again_row = 0;
			printf("Enter the Row number:\n");
			scanf("%d", &row);
			if (row<0 || row >= 4){
				printf("[Error] Row is between 0 and 3\n");
				again_row = 1;
			}
		}

		int again_column = 1;
		while (again_column){
			again_column = 0;
			printf("Enter the Column number:\n");
			scanf("%d", &column);
			if (column<0 || column >= 4){
				printf("[Error] Column is between 0 and 3\n");
				again_column = 1;
			}
		}
		if (A[row][column] != 0){
			printf("[Error] (%d,%d) is occupied\n", row, column);
			again_cell = 1;
		}
		else again_cell = 0;
	} while (again_cell);

	int again_value = 1;
	while (again_value){
		again_value = 0;
		printf("Enter the value of the cell, [2] or [4]:\n");
		scanf("%d", &value);
		if (value != 2 && value != 4){
			printf("[Error] Cell value is either 2 or 4\n");
			again_value = 1;
		}
	}

	A[row][column] = value;
	checkb(A);
	return;
}
int back_up(int D[1000][4][4], int A[4][4], int *k){
	int i, j;
	for (i = 0; i < 4; i++){
		for (j = 0; j < 4; j++){
			D[*k][i][j] = A[i][j];
			}
		}
	(*k)++;
return 0;
}

int main() {
	int mode, askagain;
	askagain = 1;
	while (askagain){
		askagain = 0;
		printf("Mode [Normal Mode (0)| Debug Mode(1)]?\n");
		scanf("%d", &mode);
		if (mode != 0 && mode != 1){
			printf("[Error] Mode is either 0 or 1\n");
		}
	}

	if (mode == 0){
		int num, row, column, repeat, seed;
		int A[4][4] = { 0 }, D[1000][4][4] = { 0 };
		int i, j, x, k=0;
		printf("Random Seed?\n");
		scanf("%d", &seed);
		srand(seed);
		randomNum(A);
		print(A);
		repeat = 0;
		do{
			x = read_command(mode);
			if (x == 5){
				printf("Bye :(\n");
				return 0;
			}
			if (x == 6){
				if (k == 0) {
					printf("[Error] You cannot undo any more!\n");
					repeat = 1;
				}
				else{
					k--;
					for (i = 0; i < 4; i++){
						for (j = 0; j < 4; j++){
							A[i][j] = D[k][i][j];
						}
					}
					print(A);
				}
			}
			else {
                back_up(D, A, &k);
				change_position(mode, x, A);
				repeat = checkb(A);
			}
		} while (repeat);
		printf("Game Over\n");
	}

	if (mode == 1){
		FILE*f1 = fopen("map.txt", "r");
		int A[4][4] = { 0 }, D[500][4][4];
		int i, j, x, repeat=1,k=0, start=0;
		for (i = 0; i<4; i++){
			for (j = 0; j<4; j++){
				fscanf(f1, "%d", &A[i][j]);
			}
		}
		fclose(f1);

		print(A);
		back_up(D, A, &k);
		do{ 
			
			Gen_Num(A);
			repeat = checkb(A);
			if (repeat == 0){
				print(A);
				break;
			}
			x = read_command(mode);
			if (x == 5){
				printf("Bye :(\n");
				return 0;
			}
				change_position(mode, x, A);
				repeat = checkb(A);
		} while (repeat);
		printf("Game Over\n");
	}
	return 0;
}
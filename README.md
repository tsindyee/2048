# 2048 in C

## Project description
This is a terminal based game of a famous game 2048.
There are two modes
- [0] is normal mode
- [1] is debug mode
Normal mode
- Game rule
    - In each round, there will be a random number generated in a random position
    - User can control the game board to move towards left, top, right, or down
    - If the numbers inside two grids collided are the same, they will combine to one grid with the sum of the number
    - Game is over when there is no more move.
- Control command
    - <img src="./2048-demo.png" width="559" height="156" />
Debug mode
- You can choose a particular position to generate a particular number instead of random generated.
- If there is any possible moves, you can use the control command to move the grids.
- After it, it will ask you to choose another place to generate a particular number.
- Debug mode will be ended if over when there is no more move.
- <img src="./2048-demo-debug.png" width="298" height="253" />
------
Project Written in 2015. By Cindy Tang.

